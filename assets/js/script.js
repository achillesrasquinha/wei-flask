function run_patch ( )
{
    const $elements = $('*[data-fieldname="response"]');
    $elements.each(function (i) {
        const $element = $($elements[i])
        const text     = $element.text()
        const json     = JSON.parse(text)
        
        $element.html(`
            <pre class="no-margin no-padding">${JSON.stringify(json, null, 4)}</pre>
        `)
    })
}

$(document).ready(function ( )
{
    const location  = window.location
    const socket    = io(location.origin)

    socket.on("connect", function ( ) {
        console.log("Socket.IO connected.")

        socket.on("model:create", function (model) {
            console.log("Model Created: " + JSON.stringify(model))

            $('.list').find('tbody').prepend(`
                <tr>
                    ${Object.keys(model).map(key => key != '_ID' && `
                        <td data-modelname="${model['_ID']}" data-fieldname="${key}">
                            ${model[key]}
                        </td>
                    `)}
                </tr>
            `)

            run_patch()
        })
    })

    run_patch()

    $('.page-actions').append(`
        <button class="page-action-clear btn btn-light">
            <small>Clear</small>
        </button>
    `)
    const $button = $('.page-actions').find('.page-action-clear')
    $button.click(function ( ) {
        console.log("Clicked Clear...")

        $.ajax({
                url: `/model`,
               type: 'DELETE',
            success: () => {
                location.reload()
            },
              error: console.log
        })
    })
})