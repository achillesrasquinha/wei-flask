import os, os.path as osp
import json
import sqlite3
from   flask import (
    Flask, render_template, jsonify, g,
    request
)
from   flask_socketio import SocketIO

def get_if_empty(a, b):
    return a if a else b

def as_list(value):
    if not isinstance(value, (list, tuple)):
        value = [value]
    return value

PATHS = dict()
PATHS['BASE']      = osp.dirname(__file__)
PATHS['TEMPLATES'] = osp.join(PATHS['BASE'], 'templates')
PATHS['ASSETS']    = osp.join(PATHS['BASE'], 'assets')
PATHS['DATABASE']  = osp.join(PATHS['BASE'], 'db.db')

class DataBase:
    def __init__(self, name, factory = None):
        self.name       = name
        self.connection = None
        self.factory    = get_if_empty(factory, DataBase.dict_factory)

    @staticmethod
    def dict_factory(cursor, row):
        dict_ = { }

        for i, column in enumerate(cursor.description):
            name        = column[0]
            dict_[name] = row[i]

        return dict_

    def connect(self):
        if not self.connection:
            self.connection             = sqlite3.connect(self.name)
            self.connection.row_factory = self.factory
    
    def sql(self, query, params = [], commit = True):
        if self.connection:
            cursor  = self.connection.cursor()
            results = cursor.execute(query, as_list(params))
            results = results.fetchall()
            
            if commit:
                self.commit()

            return results

    def commit(self):
        if self.connection:
            self.connection.commit()

    def close(self):
        if self.connection:
            self.connection.close()
            self.connection = None

def get_db(connect = True):
    db = getattr(g, '__database', None)
    if not db:
        db = g.__database = DataBase(PATHS['DATABASE'])
        if connect:
            db.connect()
            db.sql("""
                CREATE TABLE IF NOT EXISTS `tabResponse`
                (
                    _ID      INTEGER PRIMARY KEY AUTOINCREMENT,
                    response TEXT,
                    creation TIMESTAMP DEFAULT CURRENT_TIMESTAMP
                )
            """)

    return db

class Response:
    def __init__(self, status = 'success'):
        self.status = status

    def to_dict(self):
        dict_ = dict(
            status = self.status
        )
        
        return dict_

class Model:
    @staticmethod
    def get_list(name, limit = 10, page = 1):
        db      = get_db()
        results = db.sql("""
            SELECT *
            FROM   `tab{name}`
            ORDER BY creation DESC
            {limit}
        """.format(
            name  = name,
            limit = 'LIMIT {}'.format(limit) if limit else '')
            )

        return results

    @staticmethod
    def get_fields(name):
        db     = get_db()

        fields = [ ]

        cursor = db.connection.cursor()
        cursor = cursor.execute("""
            SELECT *
            FROM   `tab{name}`
        """.format(name = name))

        for column in cursor.description:
            name = column[0]
            fields.append({ "name": name })

        return fields

app      = Flask(__name__,
    template_folder = osp.join(PATHS['TEMPLATES']),
    static_folder   = osp.join(PATHS['ASSETS'])
)
socketio = SocketIO(app)

@app.route("/model", methods = ['GET', 'DELETE'])
def model(limit = None, page = 1):
    form     = request.form
    response = Response()

    if request.method == 'GET':
        name  = form.name
        limit = form.get('limit', limit)
        page  = form.get('page',  page)

        models    = Model.get_list(name,
            limit = limit,
            page  = page
        )

    if request.method == 'DELETE':
        db    = get_db()
        db.sql("""
            DELETE FROM `tabResponse`
        """)

    dict_    = response.to_dict()
    response = jsonify(dict_)

    return response
    
@app.route("/List/<model>")
def index(model):
    model = dict(
        name   = model,
        fields = Model.get_fields(model),
        data   = Model.get_list(model)
    )
    
    template = render_template('pages/list.html', model = model)
    return template

@app.route("/event", methods = ['POST'])
def event():
    response = Response()

    data     = request.get_json()
    db       = get_db()
    db.sql("""
        INSERT INTO `tabResponse`
        (response)
        VALUES
        (?)
    """, json.dumps(data))
    model    = Model.get_list('Response', limit = 1)[0]
    
    socketio.emit('model:create', model)

    dict_    = response.to_dict()
    response = jsonify(dict_)

    return response

@app.teardown_appcontext
def db_close(exception):
    db = getattr(g, '__database', None)
    if db:
        db.close()

if __name__ == '__main__':
    socketio.run(app, debug = True)