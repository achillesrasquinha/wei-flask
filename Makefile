run: install
	honcho start

install: clean
	pip install -r requirements.txt

clean:
	find . | grep -E "__pycache__|\.pyc" | xargs rm -rf

	clear